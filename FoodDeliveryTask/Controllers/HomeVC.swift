//
//  HomeVC.swift
//  FoodDeliveryTask
//
//  Created by siva chitran p on 25/01/23.
//

import UIKit
import SDWebImage

class HomeVC: UIViewController {

    @IBOutlet weak var blur_View: UIView!
    @IBOutlet weak var cartIndicatorImg: UIImageView!
    @IBOutlet weak var foodTypeCollectionView: UICollectionView!
    @IBOutlet weak var foodListTableView: UITableView!
    @IBOutlet weak var totalPrice_Lbl : UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cartTitle_Lbl: UILabel!
    @IBOutlet weak var cartTable_View: UIView!
    @IBOutlet weak var cartListTableView: UITableView!
    @IBOutlet weak var cartListTableHeightConstrain: NSLayoutConstraint!
    var homeVM = HomeVM.shared
    var cartVM = CartVM.shared
    var homeVCTableViewDataSource = HomeVCTableDataSource()
    var homeVCCollectionViewDataSource = HomeVCCollectionViewDataSource()
    var homeCartTableDataSource = HomeVCCartTableDataSource()
    var activityIndicator = UIActivityIndicatorView(style: .medium)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLoader()
        getFoodList()
    }
    
    func setUPTabelAndCollectionView(){
        self.foodListTableView.delegate = homeVCTableViewDataSource
        self.foodListTableView.dataSource = homeVCTableViewDataSource
        self.foodListTableView.register(UINib(nibName: "FoodListTableViewCell", bundle: nil), forCellReuseIdentifier: "FoodListTableViewCell")
        self.foodTypeCollectionView.delegate = homeVCCollectionViewDataSource
        self.foodTypeCollectionView.dataSource = homeVCCollectionViewDataSource
        self.foodTypeCollectionView.register(UINib(nibName: "FoodTypeCVCell", bundle: nil), forCellWithReuseIdentifier: "FoodTypeCVCell")
        self.foodTypeCollectionView.reloadData()
        self.foodListTableView.reloadData()
        self.cartListTableView.delegate = homeCartTableDataSource
        self.cartListTableView.dataSource = homeCartTableDataSource
        self.cartListTableView.register(UINib(nibName: "CartListTVCell", bundle: nil), forCellReuseIdentifier: "CartListTVCell")
    }
    
    func setupClosures(){
        homeVCTableViewDataSource.reloadFoodTable = { [weak self] foodItemIndex in
            self?.foodListTableView.reloadRows(at: [IndexPath(row: foodItemIndex, section: 0)], with: .fade)
            self?.homeVM.updateTodalOrderPrice()
        }
        
        homeVCCollectionViewDataSource.reloadFoodCollectionView = { [weak self] in
            self?.foodTypeCollectionView.reloadData()
            self?.foodListTableView.reloadData()
        }
        
        homeVM.setTotalPrice = { [weak self] in
            self?.totalPrice_Lbl.text = "\(self?.homeVM.totalOrderPrice ?? 0)"
        }
    }
    
    @IBAction func didClickCart(_ sender: Any) {
        cartVM.mapSelectedFoodItems()
        self.cartListTableView.reloadData()
        if homeVM.totalOrderPrice > 0 {
            bottomView.layer.cornerRadius = 20
            bottomView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
            self.cartIndicatorImg.image = UIImage(named: "CollapseArrow")
            self.blur_View.isHidden = false
            if bottomHeightConstraint.constant == 70 {
                view.layoutIfNeeded()
                if cartVM.foodList.count >= 3{
                    bottomHeightConstraint.constant = 380
                    cartListTableHeightConstrain.constant = 240
                }else{
                    cartListTableHeightConstrain.constant = self.cartListTableView.contentSize.height
                    bottomHeightConstraint.constant = 140 + (self.cartListTableView.contentSize.height)
                }
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                    self.cartTitle_Lbl.isHidden = false
                    self.cartTable_View.isHidden = false
                }
            }else{
                bottomView.layer.maskedCorners.remove(.layerMinXMinYCorner)
                bottomView.layer.maskedCorners.remove(.layerMaxXMinYCorner)
                    self.cartIndicatorImg.image = UIImage(named: "ExpandArrow")
                    self.blur_View.isHidden = true
                    view.layoutIfNeeded()
                    bottomHeightConstraint.constant = 70
                    UIView.animate(withDuration: 0.5) {
                        self.view.layoutIfNeeded()
                    }
                    self.cartTitle_Lbl.isHidden = true
                    self.cartTable_View.isHidden = true
                }
            } else{
            self.showAPIFailureAlert(msg: "No foodlist in cart. Please add foods.")
        }
        
        
    }
   
    func getFoodList(){
        activityIndicator.startAnimating()
        let url = "http://www.mocky.io/v2/5b700cff2e00005c009365cf"
        homeVM.getFoodList(urlString: url) { (isSuccess) in
            self.activityIndicator.stopAnimating()
            if isSuccess{
                self.setUPTabelAndCollectionView()
                self.setupClosures()
            }else{
                self.showAPIFailureAlert(msg: "Unable to fetch Food List. Please try again later.")
            }
        }
    }
    
    func showAPIFailureAlert(msg : String){
        let message = msg
        let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
        }
        self.showAlertView(message: message, actions: [okAction])
    }
    
    func setUpLoader(){
        activityIndicator.color = .lightGray
        activityIndicator.center = view.center
        activityIndicator.hidesWhenStopped = true
        self.view.addSubview(activityIndicator)
    }
}
