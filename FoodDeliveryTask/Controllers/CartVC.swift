//
//  CartVC.swift
//  FoodDeliveryTask
//
//  Created by siva chitran p on 25/01/23.
//

import UIKit

class CartVC: UIViewController {

    @IBOutlet weak var cartListATableView: UITableView!
    @IBOutlet weak var totalPrice_Lab: UILabel!
    var homeVM = HomeVM.shared
    var cartVM = CartVM.shared
    var cartvcHomeTableDataSource = CartVCTableDataSource()
    var selectedFoodType = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        totalPrice_Lab.text = "Price : \(homeVM.totalOrderPrice)"
        cartListATableView.delegate = cartvcHomeTableDataSource
        cartListATableView.dataSource = cartvcHomeTableDataSource
        cartListATableView.register(UINib(nibName: "CartTableViewCell", bundle: nil), forCellReuseIdentifier: "CartTableViewCell")
        cartVM.mapSelectedFoodItems()
    }
    
    @IBAction func didClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didClickPay(_ sender: Any) {
        
    }
}
