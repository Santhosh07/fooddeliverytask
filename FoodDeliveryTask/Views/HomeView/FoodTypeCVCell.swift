//
//  FoodTypeCVCell.swift
//  FoodDeliveryTask
//
//  Created by siva chitran p on 26/01/23.
//

import UIKit

class FoodTypeCVCell: UICollectionViewCell {

    @IBOutlet weak var highlightViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var highlightView: UIView!
    @IBOutlet weak var foodType_Lab: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
