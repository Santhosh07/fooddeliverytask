//
//  CartListTVCell.swift
//  FoodDeliveryTask
//
//  Created by siva chitran p on 27/01/23.
//

import UIKit

class CartListTVCell: UITableViewCell {

    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var foodPrive_Lbl: UILabel!
    @IBOutlet weak var foodQty_Lbl: UILabel!
    @IBOutlet weak var FAndBName_Lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        wrapperView.addBorder(width: 1, color: .lightGray, cornerRadius: 0, maskedCorner: nil)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
