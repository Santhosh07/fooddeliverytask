//
//  SubItemsCVCell.swift
//  FoodDeliveryTask
//
//  Created by siva chitran p on 26/01/23.
//

import UIKit

class SubItemsCVCell: UICollectionViewCell {

    @IBOutlet weak var subItem_View: UIView!
    @IBOutlet weak var subItem_Lab: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        subItem_View.layer.borderColor = UIColor.lightGray.cgColor
        subItem_View.layer.borderWidth = 0.7
    }

}
