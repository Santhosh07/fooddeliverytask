//
//  FoodListTableViewCell.swift
//  FoodDeliveryTask
//
//  Created by siva chitran p on 26/01/23.
//

import UIKit

protocol FoodListTableViewCellDelegate : AnyObject{
    func reloadFoodItemCell(foodItemIndex:Int,subItemIndex:Int)
    func didTapMinusBtn(foodItemIndex: Int)
    func didTapPlusBtn(foodItemIndex: Int)
}

class FoodListTableViewCell: UITableViewCell {

    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var foodName_Lab: UILabel!
    @IBOutlet weak var foodPrice_Lab: UILabel!
    @IBOutlet weak var foodQuanty_Lab: UILabel!
    @IBOutlet weak var subItemsCollectionView: UICollectionView!
    @IBOutlet weak var subItemsCollectionHeightConstraint:NSLayoutConstraint!
    weak var foodListDelegate : FoodListTableViewCellDelegate?
    var subItems = [Subitems]()
    var selectedSubItem = 0
    var cellIndex = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        setupSubItemsCollectionView()
        subItemsCollectionView.register(UINib(nibName: "SubItemsCVCell", bundle: nil), forCellWithReuseIdentifier: "SubItemsCVCell")
        wrapperView.addBorder(width: 0.7, color: .darkGray, cornerRadius: 20, maskedCorner: [.layerMinXMinYCorner,.layerMaxXMinYCorner])
        wrapperView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupSubItemsCollectionView(){
        subItemsCollectionView.delegate = self
        subItemsCollectionView.dataSource = self
    }
    
    func reloadSubItemsCollectionView(){
        subItemsCollectionView.reloadData()
        if subItems.count > 0{
            subItemsCollectionHeightConstraint.constant = 60
        }else{
            subItemsCollectionHeightConstraint.constant = 0
        }
    }
    
    @IBAction func didClickMinus(_ sender: Any) {
        foodListDelegate?.didTapMinusBtn(foodItemIndex: cellIndex)
    }
    @IBAction func didClickPlus(_ sender: Any) {
        foodListDelegate?.didTapPlusBtn(foodItemIndex: cellIndex)
    }
}

extension FoodListTableViewCell : UICollectionViewDelegate , UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let subItemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubItemsCVCell", for: indexPath) as! SubItemsCVCell
        subItemCell.subItem_Lab.text = subItems[indexPath.item].name
        if selectedSubItem == indexPath.item{
            subItemCell.subItem_View.backgroundColor = UIColor(named: "AppYellow")
            subItemCell.subItem_Lab.textColor = .black
            subItemCell.subItem_Lab.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        }else{
            subItemCell.subItem_View.backgroundColor = .clear
            subItemCell.subItem_Lab.textColor = .darkGray
            subItemCell.subItem_Lab.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        }
        
        return subItemCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if subItems.count > 2{
            return CGSize(width: collectionView.frame.size.width / 3, height: collectionView.frame.size.height)
        }else{
            return CGSize(width: collectionView.frame.size.width / 2, height: collectionView.frame.size.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        subItemsCollectionView.reloadData()
        foodListDelegate?.reloadFoodItemCell(foodItemIndex: cellIndex, subItemIndex: indexPath.item)
    }
}
