//
//  CartTableViewCell.swift
//  FoodDeliveryTask
//
//  Created by siva chitran p on 26/01/23.
//

import UIKit

class CartTableViewCell: UITableViewCell {

    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var foodPrice: UILabel!
    @IBOutlet weak var foodQuantity: UILabel!
    @IBOutlet weak var foodType: UILabel!
    @IBOutlet weak var foodname: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        wrapperView.addBorder(width: 1, color: .lightGray, cornerRadius: 0, maskedCorner: nil)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
