//
//  HomeVM.swift
//  FoodDeliveryTask
//
//  Created by siva chitran p on 26/01/23.
//

import Foundation

class HomeVM : NSObject{
    
    static let shared = HomeVM()
    
    var foodItemList = [FoodList]()
    var selectedFoodCategory = 0 //For main tab
    var maximumFoodAddQuantity = 10
    var totalOrderPrice : Float = 0
    var setTotalPrice : (()->())?
   func getFoodList(urlString: String, completion: @escaping (Bool) -> Void){

        APIManager.shared.apiRequest(urlString: urlString) { (result:Result<HomeMdl, Error>) in
            switch result{
            case .success(let resModel):
                print(resModel.foodList ?? [FoodList]())
                self.foodItemList = resModel.foodList ?? [FoodList]()
                completion(!self.foodItemList.isEmpty)
            case .failure(let error):
                print(error.localizedDescription)
                completion(false)
            }
        }
    }
    
    func updateTodalOrderPrice(){
        var price : Float = 0
        _ = foodItemList.map { foodList in
            if let fnbLists = foodList.fnblist {
                _ = fnbLists.map { fnbList in
                    if fnbList.subitems?.count ?? 0 > 0{
                        price += Float((Float(fnbList.subitems?[fnbList.selectedSubItem ?? 0].subitemPrice ?? "0") ?? 0) * Float(fnbList.foodQuantity ?? 0) )
                    }else{
                        price += (Float(fnbList.itemPrice ?? "0") ?? 0) * Float(fnbList.foodQuantity ?? 0)
                    }
                }
                
            }
        }
        totalOrderPrice = price
        self.setTotalPrice?()
    }

}
