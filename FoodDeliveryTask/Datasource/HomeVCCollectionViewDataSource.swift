//
//  HomeVCCollectionViewDataSource.swift
//  FoodDeliveryTask
//
//  Created by siva chitran p on 26/01/23.
//

import Foundation
import UIKit

class HomeVCCollectionViewDataSource : NSObject, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    let homeVM = HomeVM.shared
    
    var reloadFoodCollectionView : (()->())?
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return homeVM.foodItemList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FoodTypeCVCell", for: indexPath) as! FoodTypeCVCell
        cell.foodType_Lab.text = homeVM.foodItemList[indexPath.item].tabName?.uppercased() ?? ""
        cell.seperatorView.isHidden =  (indexPath.item == homeVM.foodItemList.count-1) ? true : false
        cell.highlightViewWidthConstraint.constant = cell.foodType_Lab.intrinsicContentSize.width
        if homeVM.selectedFoodCategory == indexPath.item {
            cell.foodType_Lab.textColor = .white
            cell.foodType_Lab.font = UIFont.systemFont(ofSize: 30.0, weight: .semibold)
            cell.highlightView.isHidden = false
        }else{
            cell.foodType_Lab.textColor = .lightGray
            cell.foodType_Lab.font = UIFont.systemFont(ofSize: 30.0, weight: .light)
            cell.highlightView.isHidden = true
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        homeVM.selectedFoodCategory = indexPath.item
        self.reloadFoodCollectionView?()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if homeVM.foodItemList.count > 2{
            return CGSize(width: collectionView.frame.size.width / 3, height: collectionView.frame.size.height)
        }else{
            return CGSize(width: collectionView.frame.size.width / 2, height: collectionView.frame.size.height)
        }
    }
    
}
