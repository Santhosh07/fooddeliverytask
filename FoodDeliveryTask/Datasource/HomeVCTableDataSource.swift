//
//  HomeVCDataSource.swift
//  FoodDeliveryTask
//
//  Created by siva chitran p on 26/01/23.
//

import Foundation
import UIKit
import SDWebImage
class HomeVCTableDataSource : NSObject, UITableViewDelegate,UITableViewDataSource{
    
    let homeVM = HomeVM.shared
    var cartVM = CartVM.shared
    
    var reloadFoodTable : ((_ fooItemIndex:Int)->())?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return homeVM.foodItemList[homeVM.selectedFoodCategory].fnblist?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FoodListTableViewCell", for: indexPath) as! FoodListTableViewCell
        if let foodItems = homeVM.foodItemList[homeVM.selectedFoodCategory].fnblist {
            let foodItem = foodItems[indexPath.row]
            cell.foodName_Lab.text = foodItem.name?.uppercased() ?? ""
            cell.foodPrice_Lab.text = foodItem.subItemCount ?? 0 > 0 ? foodItem.subitems?[foodItem.selectedSubItem ?? 0].subitemPrice ?? "" : foodItem.itemPrice ?? ""
            cell.imgView.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imgView.sd_imageTransition = SDWebImageTransition.fade
            cell.imgView.sd_setImage(with: URL(string: foodItem.imageUrl ?? ""), placeholderImage: UIImage(named: "SampleImg"))
            cell.foodQuanty_Lab.text = "\(foodItem.foodQuantity ?? 0)"
            cell.subItems = foodItem.subitems ?? [Subitems]()
            cell.cellIndex = indexPath.row
            cell.selectedSubItem = foodItem.selectedSubItem ?? 0
            cell.reloadSubItemsCollectionView()
            cell.foodListDelegate = self
        }
        return cell
    }
}



extension HomeVCTableDataSource : FoodListTableViewCellDelegate{
    
    func didTapMinusBtn(foodItemIndex: Int) {
        var qty = homeVM.foodItemList[homeVM.selectedFoodCategory].fnblist?[foodItemIndex].foodQuantity ?? 0
        
        if qty != 0{
            qty -= 1
            homeVM.foodItemList[homeVM.selectedFoodCategory].fnblist?[foodItemIndex].foodQuantity = qty
            self.reloadFoodTable?(foodItemIndex)
        }
    }
    
    func didTapPlusBtn(foodItemIndex: Int) {
        var qty = homeVM.foodItemList[homeVM.selectedFoodCategory].fnblist?[foodItemIndex].foodQuantity ?? 0
    
        if qty != homeVM.maximumFoodAddQuantity{
            qty += 1
            homeVM.foodItemList[homeVM.selectedFoodCategory].fnblist?[foodItemIndex].foodQuantity = qty
            self.reloadFoodTable?(foodItemIndex)
        }
    }
    
    func reloadFoodItemCell(foodItemIndex: Int, subItemIndex: Int) {
        homeVM.foodItemList[homeVM.selectedFoodCategory].fnblist?[foodItemIndex].selectedSubItem = subItemIndex
        self.reloadFoodTable?(foodItemIndex)
    }
}
