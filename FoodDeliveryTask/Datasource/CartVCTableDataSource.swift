//
//  CartVCTableDataSource.swift
//  FoodDeliveryTask
//
//  Created by siva chitran p on 26/01/23.
//

import Foundation
import UIKit

class CartVCTableDataSource : NSObject,UITableViewDelegate , UITableViewDataSource{
    
    var cartVM = CartVM.shared
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartVM.foodList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartTableViewCell", for: indexPath) as! CartTableViewCell
         let cartData = cartVM.foodList[indexPath.row]
        cell.foodname.text = cartData.name?.capitalized
        cell.imgView.sd_setImage(with: URL(string: cartData.imgUrl ?? ""), placeholderImage: UIImage(named: "SampleImg"))
            cell.foodQuantity.text = "Qty: \(cartData.foodQuantity ?? 0)"
        if cartData.subItemCount ?? 0 > 0 {
            cell.foodType.text = cartData.subitems?[cartData.selectedSubItem ?? 0].name?.capitalized ?? ""
            cell.foodPrice.text = "\((Float(cartData.subitems?[cartData.selectedSubItem ?? 0].subitemPrice ?? "0") ?? 0) * Float(cartData.foodQuantity ?? 0))"
        }else{
            cell.foodType.text = ""
            cell.foodPrice.text = "\((Float(cartData.itemPrice ?? "0") ?? 0) * Float(cartData.foodQuantity ?? 0))"
        }
        
        return cell
    }
}
