//
//  HomeVCCartTableDataSource.swift
//  FoodDeliveryTask
//
//  Created by siva chitran p on 27/01/23.
//

import Foundation
import UIKit

class HomeVCCartTableDataSource : NSObject,UITableViewDelegate , UITableViewDataSource{
    
    var cartVM = CartVM.shared
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartVM.foodList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartListTVCell", for: indexPath) as! CartListTVCell
         let cartData = cartVM.foodList[indexPath.row]
        
        cell.FAndBName_Lbl.text = cartData.name?.uppercased()
        cell.foodQty_Lbl.text = "Qty: \(cartData.foodQuantity ?? 0)"
        if cartData.subItemCount ?? 0 > 0 {
            cell.foodPrive_Lbl.text = "\((Float(cartData.subitems?[cartData.selectedSubItem ?? 0].subitemPrice ?? "0") ?? 0) * Float(cartData.foodQuantity ?? 0))"
        }else{
            cell.foodPrive_Lbl.text = "\((Float(cartData.itemPrice ?? "0") ?? 0) * Float(cartData.foodQuantity ?? 0))"
        }
        return cell
    }
}
