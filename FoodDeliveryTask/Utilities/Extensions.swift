//
//  Extensions.swift
//  FoodDeliveryTask
//
//  Created by siva chitran p on 26/01/23.
//

import Foundation
import UIKit

extension UIViewController{
    func showAlertView(message: String, actions: [UIAlertAction]){
        let alertController = UIAlertController(title: "FoodDeliveryTask", message: message, preferredStyle: .alert)
        for action in actions{
            alertController.addAction(action)
        }
        self.present(alertController, animated: true, completion: nil)
    }
}
extension UIView{
    func addBorder(width: CGFloat, color : UIColor, cornerRadius : CGFloat, maskedCorner :CACornerMask?){
        self.layer.cornerRadius = cornerRadius
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
        if let maskedCorner = maskedCorner {
            self.layer.maskedCorners = maskedCorner
        }
        
    }
}


