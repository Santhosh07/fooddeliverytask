//
//  APIManager.swift
//  FoodDeliveryTask
//
//  Created by siva chitran p on 26/01/23.
//

import Foundation
import UIKit
import Alamofire

class  APIManager {
    
    static let shared = APIManager()
    
    private init(){}
    
    func apiRequest<T: Codable>(urlString: String, method: HTTPMethod = .get, parameters: [String: Any]? = nil, encoding: ParameterEncoding = JSONEncoding.default, headers: HTTPHeaders? = nil, completion: @escaping (Result<T, Error>) -> Void){
        AF.request(urlString,method: method, parameters: parameters,encoding: encoding, headers: headers).responseData(completionHandler: {response in
            switch response.result{
            case .success(let res):
                do {
                    completion(.success(try JSONDecoder().decode(T.self, from: res)))
                } catch let error {
                    print(String(data: res, encoding: .utf8) ?? "nothing received")
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        })
     }
}
