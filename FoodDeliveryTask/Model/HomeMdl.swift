//
//  HomeMdl.swift
//  FoodDeliveryTask
//
//  Created by siva chitran p on 26/01/23.
//

import Foundation

struct HomeMdl : Codable {
    let status : Status?
    let currency : String?
    let foodList : [FoodList]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case currency = "Currency"
        case foodList = "FoodList"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Status.self, forKey: .status)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        foodList = try values.decodeIfPresent([FoodList].self, forKey: .foodList)
    }

}

struct Status : Codable {
    let id : String?
    let description : String?

    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case description = "Description"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        description = try values.decodeIfPresent(String.self, forKey: .description)
    }

}

struct FoodList : Codable {
    let tabName : String?
    let fnblist : [Fnblist]?

    enum CodingKeys: String, CodingKey {
        case tabName = "TabName"
        case fnblist = "fnblist"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        tabName = try values.decodeIfPresent(String.self, forKey: .tabName)
        fnblist = try values.decodeIfPresent([Fnblist].self, forKey: .fnblist)
    }
}

class Fnblist : Codable {
    let cinemaid : String?
    let tabName : String?
    let vistaFoodItemId : String?
    let name : String?
    let priceInCents : String?
    let itemPrice : String?
    let sevenStarExperience : String?
    let otherExperience : String?
    let subItemCount : Int?
    let imageUrl : String?
    let imgUrl : String?
    let vegClass : String?
    let subitems : [Subitems]?
    var selectedSubItem : Int?
    var foodQuantity : Int?
    enum CodingKeys: String, CodingKey {
        case cinemaid = "Cinemaid"
        case tabName = "TabName"
        case vistaFoodItemId = "VistaFoodItemId"
        case name = "Name"
        case priceInCents = "PriceInCents"
        case itemPrice = "ItemPrice"
        case sevenStarExperience = "SevenStarExperience"
        case otherExperience = "OtherExperience"
        case subItemCount = "SubItemCount"
        case imageUrl = "ImageUrl"
        case imgUrl = "ImgUrl"
        case vegClass = "VegClass"
        case subitems = "subitems"
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        cinemaid = try values.decodeIfPresent(String.self, forKey: .cinemaid)
        tabName = try values.decodeIfPresent(String.self, forKey: .tabName)
        vistaFoodItemId = try values.decodeIfPresent(String.self, forKey: .vistaFoodItemId)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        priceInCents = try values.decodeIfPresent(String.self, forKey: .priceInCents)
        itemPrice = try values.decodeIfPresent(String.self, forKey: .itemPrice)
        sevenStarExperience = try values.decodeIfPresent(String.self, forKey: .sevenStarExperience)
        otherExperience = try values.decodeIfPresent(String.self, forKey: .otherExperience)
        subItemCount = try values.decodeIfPresent(Int.self, forKey: .subItemCount)
        imageUrl = try values.decodeIfPresent(String.self, forKey: .imageUrl)
        imgUrl = try values.decodeIfPresent(String.self, forKey: .imgUrl)
        vegClass = try values.decodeIfPresent(String.self, forKey: .vegClass)
        subitems = try values.decodeIfPresent([Subitems].self, forKey: .subitems)
        if subitems?.count ?? 0 > 0{
            selectedSubItem = 0
        }
        foodQuantity = 0
    }

}

struct Subitems : Codable {
    let name : String?
    let priceInCents : String?
    let subitemPrice : String?
    let vistaParentFoodItemId : String?
    let vistaSubFoodItemId : String?

    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case priceInCents = "PriceInCents"
        case subitemPrice = "SubitemPrice"
        case vistaParentFoodItemId = "VistaParentFoodItemId"
        case vistaSubFoodItemId = "VistaSubFoodItemId"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        priceInCents = try values.decodeIfPresent(String.self, forKey: .priceInCents)
        subitemPrice = try values.decodeIfPresent(String.self, forKey: .subitemPrice)
        vistaParentFoodItemId = try values.decodeIfPresent(String.self, forKey: .vistaParentFoodItemId)
        vistaSubFoodItemId = try values.decodeIfPresent(String.self, forKey: .vistaSubFoodItemId)
    }

}
